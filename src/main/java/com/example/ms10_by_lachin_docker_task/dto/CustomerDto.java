package com.example.ms10_by_lachin_docker_task.dto;

import lombok.Data;

@Data
public class CustomerDto {

    private Long id;
    private String name;
    private String surName;
}
