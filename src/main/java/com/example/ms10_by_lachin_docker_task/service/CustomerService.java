package com.example.ms10_by_lachin_docker_task.service;

import com.example.ms10_by_lachin_docker_task.dto.CustomerDto;
import com.example.ms10_by_lachin_docker_task.dto.CustomerRequestDto;
import com.example.ms10_by_lachin_docker_task.entity.Customer;

import java.util.List;

public interface CustomerService {
    Customer getById(Long id);

    CustomerDto createCustomer(CustomerRequestDto customerRequestDto);

    Customer updateCustomer(Long id, Customer customer);

    void deleteCustomer(Long id);

    List<Customer> getCustomerList();
}
