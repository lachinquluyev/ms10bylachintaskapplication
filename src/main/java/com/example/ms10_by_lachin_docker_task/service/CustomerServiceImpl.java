package com.example.ms10_by_lachin_docker_task.service;

import com.example.ms10_by_lachin_docker_task.dto.CustomerDto;
import com.example.ms10_by_lachin_docker_task.dto.CustomerRequestDto;
import com.example.ms10_by_lachin_docker_task.entity.Customer;
import com.example.ms10_by_lachin_docker_task.repo.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService{

    private final ModelMapper mapper;
    private final CustomerRepository repository;


    @Override
    public Customer getById(Long id) {
        return repository.findById(id)
                .orElseThrow(() -> new RuntimeException("Customer not found"));
    }

    @Override
    public CustomerDto createCustomer(CustomerRequestDto customerRequestDto) {
        Customer customer = mapper.map(customerRequestDto, Customer.class);
        return mapper.map(repository.save(customer), CustomerDto.class);
    }

    @Override
    public Customer updateCustomer(Long id, Customer customer) {
      repository.findById(id)
                .orElseThrow(() -> new RuntimeException("Customer not found"));
      customer.setId(id);
      return repository.save(customer);
    }

    @Override
    public void deleteCustomer(Long id) {
        repository.findById(id)
                .orElseThrow(() -> new RuntimeException("Customer not found"));
        repository.deleteById(id);
    }

    @Override
    public List<Customer> getCustomerList() {
        return repository.findAll();
    }
}
