package com.example.ms10_by_lachin_docker_task;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ms10ByLachinDockerTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ms10ByLachinDockerTaskApplication.class, args);
	}

}
