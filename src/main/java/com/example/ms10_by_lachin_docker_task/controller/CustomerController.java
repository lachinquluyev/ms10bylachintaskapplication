package com.example.ms10_by_lachin_docker_task.controller;

import com.example.ms10_by_lachin_docker_task.dto.CustomerDto;
import com.example.ms10_by_lachin_docker_task.dto.CustomerRequestDto;
import com.example.ms10_by_lachin_docker_task.entity.Customer;
import com.example.ms10_by_lachin_docker_task.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customers")
@RequiredArgsConstructor
public class CustomerController {

    private final CustomerService service;

    @GetMapping("/hello")
    public String sayHello(){
        return "Hello Spring!";
    }

    @GetMapping("/{id}")
    public Customer getCustomer(@PathVariable Long id) {
        return service.getById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CustomerDto createCustomer(@RequestBody CustomerRequestDto customerRequestDto) {
        return service.createCustomer(customerRequestDto);
    }

    @PutMapping("/{id}")
    public Customer updateCustomer(@PathVariable Long id, @RequestBody Customer customer) {
        return service.updateCustomer(id, customer);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCustomer(@PathVariable Long id) {
        service.deleteCustomer(id);
    }
    @GetMapping("/list")
    public List<Customer> getCustomerList() {
        return service.getCustomerList();
    }
}
