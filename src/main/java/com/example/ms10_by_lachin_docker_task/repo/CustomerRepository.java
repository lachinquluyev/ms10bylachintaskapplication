package com.example.ms10_by_lachin_docker_task.repo;

import com.example.ms10_by_lachin_docker_task.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer,Long> {

}
