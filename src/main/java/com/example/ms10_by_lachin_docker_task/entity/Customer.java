package com.example.ms10_by_lachin_docker_task.entity;

import jakarta.persistence.*;
import lombok.Data;


@Entity
@Data
@Table(name = "customers")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private String surName;
}
